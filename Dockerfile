FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9300

COPY target/*.jar getdata.jar
COPY keystore.p12 keystore.p12

ENTRYPOINT [ "java","-jar","./getdata.jar"]