package com.example.getdata.service;

import com.example.getdata.dto.request.RequestDTO;
import com.example.getdata.model.DailyData;
import com.example.getdata.model.PracticeData;
import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.response.FitnessData;
import com.example.getdata.service.daily.GetHeartRateDataDaily;
import com.example.getdata.service.daily.GetSleepDataService;
import com.example.getdata.service.practicetime.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;


@Service

public class GetDataService {

    @Autowired
    @Qualifier("restTemplate1")
    RestTemplate restTemplate1;
    @Autowired
    GetDistanceDataService distanceService;
    @Autowired
    GetMaxSpeedDataService maxSpeedService;
    @Autowired
    GetBurnedCaloriesDataService caloriesService;
    @Autowired
    GetSpeedInSecondsDataService speedInSecondsService;
    @Autowired
    GetHeartRateDataService heartRateService;
    @Autowired
    GetHeartRateDataDaily heartRateDailyService;
    @Autowired
    GetSleepDataService sleepDataService;

    @Autowired
    RabbitTemplate rabbitTemplate;
    @Value("${practicedata.rabbitmq.exchange}")
    private String exchangePracticeData;
    @Value("${practicedata.rabbitmq.routingkey}")
    private String routingkeyPracticeData;
    @Value("${dailydata.rabbitmq.exchange}")
    private String exchangeDailyData;
    @Value("${dailydata.rabbitmq.routingkey}")
    private String routingKeyDailyData;



    public void getData(String accessToken,RequestDTO request) {
        String requestUri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " + accessToken);

        PracticeTime practiceTime = getPracticeTime(request.getTeamID());
        System.out.println("practice= "+practiceTime.getStartTimeMillis());
        getPracticeData(headers,requestUri,practiceTime,request);
        getDailyData(headers,requestUri,practiceTime,request);

    }
    public void getPracticeData(HttpHeaders headers,String requestUri,PracticeTime practiceTime,RequestDTO request){
        FitnessData speedInSecondsData = speedInSecondsService.getSpeedInSecondsPracticeTime(practiceTime, requestUri, headers);
        FitnessData heartRateData = heartRateService.getHeartRateInPracticeTime(practiceTime, requestUri, headers);
        FitnessData heartRateEveryMinute = heartRateService.getHeartRateEveryMinuteInPracticeTime(practiceTime, requestUri, headers);
        PracticeData practiceData = new PracticeData();
        practiceData.setTotalDistance(
                distanceService.getTotalDistanceInPracticeTime(practiceTime, requestUri, headers));
        practiceData.setMaxSpeed(
                (maxSpeedService.getMaxSpeedInPracticeTime(practiceTime, requestUri, headers)));
        practiceData.setBurnedCalories(
                caloriesService.getTotalBurnedCaloriesForDay(practiceTime, requestUri, headers));
        practiceData.setNumOfSprints(
                speedInSecondsService.getNumOfSprintsInPracticeTime(speedInSecondsData));
        practiceData.setHsr(
                ((double) speedInSecondsService.getHSRInPracticeTime(speedInSecondsData)) / 60);// convert from seconds to minutes
        practiceData.setAvgHeartRate(
                heartRateService.getAvgHeartRateInPracticeTime(heartRateData));
        practiceData.setMaxHeartRate(
                heartRateService.getMaxHeartRateInPracticeTime(heartRateData));
        practiceData.setMinHeartRate(
                heartRateService.getMinHeartRateInPracticeTime(heartRateData));
        practiceData.setRedZoneDuration(
                heartRateService.getRedZoneDurationInPracticeTime(heartRateEveryMinute));
        practiceData.setPlayerID(request.getPlayerID());
        practiceData.setTeamID(request.getTeamID());
        practiceData.setStartTimeMillis(practiceTime.getStartTimeMillis());
        practiceData.setEndTimeMillis(practiceTime.getEndTimeMillis());

        rabbitTemplate.convertAndSend(exchangePracticeData,routingkeyPracticeData, practiceData);
    }
    public void getDailyData(HttpHeaders headers,String requestUri,PracticeTime practiceTime,RequestDTO request){
        long todayDate = normalizeOneDayDate(practiceTime);
        FitnessData heartRateDaily = heartRateDailyService.getHeartRateDaily(todayDate,requestUri,headers);
        List<Double> zones = heartRateDailyService.getHeartRateDailyZones(heartRateDaily);
        double sleep = sleepDataService.getSleepDataForDay(todayDate,requestUri,headers);

        DailyData dailyData =new DailyData();
        dailyData.setPlayerID(request.getPlayerID());
        dailyData.setTeamID(request.getTeamID());
        dailyData.setStartTimeMillis(todayDate);
        dailyData.setEndTimeMillis(todayDate+ + 86400000);
        dailyData.setRelaxedHeartRate(zones.get(0));
        dailyData.setLightIntensityHeartRate(zones.get(1));
        dailyData.setModerateIntensityHeartRate(zones.get(2));
        dailyData.setVigorousIntensityHeartRate(zones.get(3));
        dailyData.setSleepHours(sleep);

        rabbitTemplate.convertAndSend(exchangeDailyData,routingKeyDailyData, dailyData);
    }

    public long normalizeOneDayDate(PracticeTime practiceTime){
        Date date = new Date(practiceTime.getStartTimeMillis());
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date.getTime();
    }

    public PracticeTime getPracticeTime(long teamID){
        String url = String.format("http://practice-time/api/v1/practice/get/%o",teamID);
        return restTemplate1.getForObject(url,PracticeTime.class);
    }
}















/* public List<FitnessData> getTotalDistance(String accessToken, RequestAggregatedData request){
        String requestUri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " + accessToken);

        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.distance.delta"));
        request.setAggregateBy(agg);
        long startTime=1651870800000L;
        request.setBucketByTime(new DurationMillis(86400000L));

        List<FitnessData> fitnessDataList =new ArrayList<>();
        do{
            request.setStartTimeMillis(startTime);
            request.setEndTimeMillis(startTime+86400000L);
            HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

            long start = System.currentTimeMillis();
            ResponseEntity<FitnessData> response = restTemplate.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
            long end = System.currentTimeMillis();

            System.out.println("GetDataService" + (end - start));
            System.out.println();
            fitnessDataList.add(response.getBody());

            startTime+=86400000L;
        }while (startTime <System.currentTimeMillis());

        return fitnessDataList;
    }*/
