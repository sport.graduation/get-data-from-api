package com.example.getdata.service.daily;

import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.primitives.*;
import com.example.getdata.dto.request.RequestAggregatedData;
import com.example.getdata.dto.response.FitnessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service

public class GetHeartRateDataDaily {
    @Autowired
    @Qualifier("restTemplate2")
    RestTemplate rest;


    public FitnessData getHeartRateDaily(long todayDate, String requestUri, HttpHeaders headers) {

        RequestAggregatedData request = buildHeartRateDailyRequest(todayDate);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetHeartRateDailyDataService " + (end - start));

        return response.getBody();
    }

    public List<Double> getHeartRateDailyZones (FitnessData heartRateDaily){

        List<Double> zones = new ArrayList<>();
        int relaxedHeartRate=0;
        int lightIntensityHeartRate=0;
        int moderateIntensityHeartRate=0;
        int vigorousIntensityHeartRate=0;

        for (Bucket bucket : heartRateDaily.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    if (point.getValue().get(0).getFpVal() >= 0 && point.getValue().get(0).getFpVal() <= 60)
                        relaxedHeartRate+=10;
                    else if (point.getValue().get(0).getFpVal() > 60 && point.getValue().get(0).getFpVal() <= 100)
                        lightIntensityHeartRate+=10;
                    else if (point.getValue().get(0).getFpVal() > 100 && point.getValue().get(0).getFpVal() <= 150)
                        moderateIntensityHeartRate+=10;
                    else if (point.getValue().get(0).getFpVal() > 150)
                        vigorousIntensityHeartRate+=10;
                }
            }
        }
        zones.add(0, (double) (relaxedHeartRate/60));
        zones.add(1, (double) (lightIntensityHeartRate/60));
        zones.add(2, (double) (moderateIntensityHeartRate/60));
        zones.add(3, (double) (vigorousIntensityHeartRate/60));

        return zones;
    }


    public RequestAggregatedData buildHeartRateDailyRequest(long todayDate) {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.heart_rate.bpm"));
        request.setAggregateBy(agg);
        request.setStartTimeMillis(todayDate);
        request.setEndTimeMillis(todayDate + 86400000);

        request.setBucketByTime(new DurationMillis(600000));

        return request;
    }
}





