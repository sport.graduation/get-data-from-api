package com.example.getdata.service.daily;

import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.primitives.*;
import com.example.getdata.dto.request.RequestAggregatedData;
import com.example.getdata.dto.response.FitnessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GetSleepDataService {
    @Autowired
    @Qualifier("restTemplate2")

    RestTemplate rest;

    public double getSleepDataForDay(long todayDate, String requestUri, HttpHeaders headers) {

        RequestAggregatedData request = buildSleepDataRequest(todayDate);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetSleepDataService " + (end - start));

        double sleepDuration = 0;
        FitnessData sleepData = response.getBody();
        System.out.println(response.getBody());
        for (Bucket bucket : sleepData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    if (point.getValue().get(0).getIntVal() == 2 || point.getValue().get(0).getIntVal() == 4 ||
                        point.getValue().get(0).getIntVal() == 5 || point.getValue().get(0).getIntVal() == 6){
                        sleepDuration += ((point.getEndTimeNanos()-point.getStartTimeNanos())/3600000000000.0);
                    }
                }
            }
        }
        return sleepDuration;
    }



    public RequestAggregatedData buildSleepDataRequest(long todayDate) {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.sleep.segment"));
        request.setAggregateBy(agg);
        request.setStartTimeMillis(todayDate - 10800000);
        request.setEndTimeMillis(todayDate + 43200000);
        System.out.println("sleep start= " +request.getStartTimeMillis());
        System.out.println("sleep end= " + request.getEndTimeMillis());
        return request;
    }
}
