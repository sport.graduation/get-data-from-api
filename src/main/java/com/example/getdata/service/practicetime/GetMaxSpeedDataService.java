package com.example.getdata.service.practicetime;

import com.example.getdata.dto.primitives.*;
import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.request.RequestAggregatedData;
import com.example.getdata.dto.response.FitnessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetMaxSpeedDataService {

    @Autowired
    @Qualifier("restTemplate2")
    RestTemplate rest;

    public double getMaxSpeedInPracticeTime( PracticeTime practiceTime, String requestUri, HttpHeaders headers) {

        RequestAggregatedData request = buildMaxSpeedDataRequest(practiceTime);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetMaxSpeedDataService " + (end - start));

        double maxSpeed = 0;
        FitnessData fitnessData = response.getBody();

        for (Bucket bucket : fitnessData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    maxSpeed = point.getValue().get(1).getFpVal();
                }
            }
        }

        return maxSpeed;
    }

    public RequestAggregatedData buildMaxSpeedDataRequest(PracticeTime practiceTime)
    {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.speed"));
        request.setAggregateBy(agg);

        request.setStartTimeMillis(practiceTime.getStartTimeMillis());
        request.setEndTimeMillis(practiceTime.getEndTimeMillis());
        request.setBucketByTime(new DurationMillis(request.getEndTimeMillis() - request.getStartTimeMillis()));

        return request;
    }
}
