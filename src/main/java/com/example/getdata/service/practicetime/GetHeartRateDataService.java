package com.example.getdata.service.practicetime;

import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.primitives.*;
import com.example.getdata.dto.request.RequestAggregatedData;
import com.example.getdata.dto.response.FitnessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetHeartRateDataService {

    @Autowired
    @Qualifier("restTemplate2")

    RestTemplate rest;

    private final double HighHeartRateLimit = 180 ; // HeartRate above this limit consider in the red zone

    public FitnessData getHeartRateInPracticeTime(PracticeTime practiceTime, String requestUri, HttpHeaders headers) {

        RequestAggregatedData request = buildHeartRateDataRequest(practiceTime);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetHeartRateDataService " + (end - start));

        return response.getBody();
    }
    public FitnessData getHeartRateEveryMinuteInPracticeTime(PracticeTime practiceTime, String requestUri, HttpHeaders headers) {
        RequestAggregatedData request = buildHeartRateDataEveryMinuteRequest(practiceTime);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetHeartRateEveryMinuteDataService " + (end - start));

        return response.getBody();
    }

    public int getAvgHeartRateInPracticeTime (FitnessData heartRateData){
        int avgHeartRate = 0;
        for (Bucket bucket : heartRateData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    avgHeartRate = (int) point.getValue().get(0).getFpVal();
                }
            }
        }
        return avgHeartRate;
    }
    public int getMaxHeartRateInPracticeTime (FitnessData heartRateData){
        int maxHeartRate = 0;
        for (Bucket bucket : heartRateData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    maxHeartRate = (int) point.getValue().get(1).getFpVal();
                }
            }
        }
        return maxHeartRate;
    }
    public int getMinHeartRateInPracticeTime (FitnessData heartRateData){
        int minHeartRate = 0;
        for (Bucket bucket : heartRateData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    minHeartRate = (int) point.getValue().get(2).getFpVal();
                }
            }
        }
        return minHeartRate;
    }
    public int getRedZoneDurationInPracticeTime(FitnessData heartRateEveryMinuteData){
        int redZoneDuration =0;

        for (Bucket bucket : heartRateEveryMinuteData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    if (point.getValue().get(0).getFpVal() > HighHeartRateLimit)
                        redZoneDuration++;
                }
            }
        }
        return redZoneDuration;
    }

    public RequestAggregatedData buildHeartRateDataRequest(PracticeTime practiceTime) {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.heart_rate.bpm"));
        request.setAggregateBy(agg);

        request.setStartTimeMillis(practiceTime.getStartTimeMillis());
        request.setEndTimeMillis(practiceTime.getEndTimeMillis());
        request.setBucketByTime(new DurationMillis(practiceTime.getEndTimeMillis() - practiceTime.getStartTimeMillis()));

        return request;
    }
    public RequestAggregatedData buildHeartRateDataEveryMinuteRequest(PracticeTime practiceTime) {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.heart_rate.bpm"));
        request.setAggregateBy(agg);

        request.setStartTimeMillis(practiceTime.getStartTimeMillis());
        request.setEndTimeMillis(practiceTime.getEndTimeMillis());
        request.setBucketByTime(new DurationMillis(60000));

        return request;
    }
}


