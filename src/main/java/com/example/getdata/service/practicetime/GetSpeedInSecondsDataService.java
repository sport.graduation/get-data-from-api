package com.example.getdata.service.practicetime;

import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.primitives.*;
import com.example.getdata.dto.request.RequestAggregatedData;
import com.example.getdata.dto.response.FitnessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetSpeedInSecondsDataService {

    @Autowired
    @Qualifier("restTemplate2")

    RestTemplate rest;

    private final double highSpeedLimit = 5 ; // a speed ratio of 5 m/s and higher consider "high speed running"

    public FitnessData getSpeedInSecondsPracticeTime(PracticeTime practiceTime, String requestUri, HttpHeaders headers) {

        RequestAggregatedData request = buildSpeedDataRequest(practiceTime);
        HttpEntity<RequestAggregatedData> requestEntity = new HttpEntity<>(request, headers);

        long start = System.currentTimeMillis();
        ResponseEntity<FitnessData> response = rest.exchange(requestUri, HttpMethod.POST, requestEntity, FitnessData.class);
        long end = System.currentTimeMillis();
        System.out.println("GetSpeedInSecondsDataService " + (end - start));

        return response.getBody();
    }

    public int getNumOfSprintsInPracticeTime (FitnessData speedInSecondsData){
        int numOfSprints = 0;
        for (Bucket bucket : speedInSecondsData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    if (point.getValue().get(1).getFpVal() > highSpeedLimit)
                        numOfSprints++;
                }
            }
        }
        return numOfSprints;
    }

    public int getHSRInPracticeTime (FitnessData speedInSecondsData){
        int hsrDuration = 0;
        for (Bucket bucket : speedInSecondsData.getBucket()) {
            for (Dataset dataset : bucket.getDataset()) {
                for (Point point : dataset.getPoint()) {
                    if (point.getValue().get(0).getFpVal() > highSpeedLimit)
                        hsrDuration+=30;
                }
            }
        }
        return hsrDuration;
    }

    public RequestAggregatedData buildSpeedDataRequest(PracticeTime practiceTime)
    {
        RequestAggregatedData request = new RequestAggregatedData();
        List<DataTypeName> agg = new ArrayList<>();
        agg.add(new DataTypeName("com.google.speed"));
        request.setAggregateBy(agg);

        //Date date = new Date(System.currentTimeMillis() - 86400000);
        //date.setHours(practiceTime.getStartingHour());
        //date.setMinutes(practiceTime.getStartingMinute());
        //request.setStartTimeMillis(date.getTime());

        //date.setHours(practiceTime.getEndingHour());
        //date.setMinutes(practiceTime.getEndingMinute());
        //request.setEndTimeMillis(date.getTime());
        request.setStartTimeMillis(practiceTime.getStartTimeMillis());
        request.setEndTimeMillis(practiceTime.getEndTimeMillis());
        request.setBucketByTime(new DurationMillis(30000));

        return request;
    }
}
