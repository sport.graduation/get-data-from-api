package com.example.getdata.configuration;

public class FitnessAccessDeniedException extends RuntimeException {
    public FitnessAccessDeniedException(String message)  {
        super(message);
    }
}
