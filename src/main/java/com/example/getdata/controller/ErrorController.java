package com.example.getdata.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/getdata")
public class ErrorController {

    @GetMapping("/unauthorized")
    public String unauthorized() {
        return "errors/unauthorized";
    }

    @PostMapping("/unauthorized")
    public String unmnauthorized() {
        return "errors/unauthorized";
    }
}
