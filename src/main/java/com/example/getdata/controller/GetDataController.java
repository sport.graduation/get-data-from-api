package com.example.getdata.controller;

import com.example.getdata.configuration.FitnessAccessDeniedException;
import com.example.getdata.configuration.SessionKey;
import com.example.getdata.dto.PracticeTime;
import com.example.getdata.dto.request.RequestDTO;
import com.example.getdata.service.GetDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.List;


@RestController
@RequestMapping("/api/v1/getdata")
public class GetDataController {


    @Autowired
    GetDataService getDataService;

    @Autowired
    @Qualifier("restTemplate1")
    RestTemplate restTemplate1;

    @PostMapping(value = "/sync")
    public String getDataFromFitAPIWithPost(HttpSession session, Model model, @RequestBody RequestDTO requestDTO) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();
        if (accessToken == null || accessToken.isBlank()) {
            throw new FitnessAccessDeniedException("Invalid token");
        }
        getDataService.getData(accessToken, requestDTO);
        return "done";
    }

    @GetMapping(value = "/sync")
    public String getDataFromFitAPIWithGet(HttpSession session, Model model) {
        String accessToken = session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()) == null ? "" : session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()).toString();
        if (accessToken == null || accessToken.isBlank()) {
            throw new FitnessAccessDeniedException("Invalid token");
        }
        RequestDTO requestDTO = new RequestDTO(1, 1);
        getDataService.getData(accessToken, requestDTO);
        return "done";
    }


    @PostMapping("/to")
    public String toDelete1(HttpSession session, @RequestBody RequestDTO requestDTO) {
        System.out.println(session.toString());
        System.out.println("session token= " + session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()));
        System.out.println(session.getCreationTime());
        return "done";
    }


    @GetMapping("/to")
    public String toDelete2(HttpSession session) {
        System.out.println(session.toString());
        System.out.println("session token= " + session.getAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString()));
        System.out.println(session.getCreationTime());
        return "done";
    }

    @GetMapping("/test")
    public String toDeelete2(){
        return restTemplate1.getForObject("http://practice-time/api/v1/practice/hi", String.class);
    }

    @GetMapping(value = "/hi")
    public String hi(){
        return "hi";
    }

    @GetMapping(value = "/go")
    public String huhuki(){
        return "done";
    }
}