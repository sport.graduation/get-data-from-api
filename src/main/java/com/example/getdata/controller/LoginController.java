package com.example.getdata.controller;

import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/api/v1/getdata")
public class LoginController {
    //private static final String authorizationRequestBaseUri = "oauth2/authorization";
    Map<String, String> oauth2AuthenticationUrls = new HashMap<>();
    private final ClientRegistrationRepository clientRegistrationRepository;

    public LoginController(ClientRegistrationRepository clientRegistrationRepository) {
        this.clientRegistrationRepository = clientRegistrationRepository;
    }

    @RequestMapping("/login")
    public String login(Model model) {
        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository).as(Iterable.class);
        if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }

       /* clientRegistrations.forEach(
                registration -> oauth2AuthenticationUrls.put(
                        registration.getClientName(),
                        authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
*/
        String s = "/oauth2/authorization/google";
        clientRegistrations.forEach(
                registration -> oauth2AuthenticationUrls.put(registration.getClientName(),s));
        model.addAttribute("urls", "https://ec2-157-175-145-254.me-south-1.compute.amazonaws.com:9300/oauth2/authorization/google");

        return "login";
    }
}
