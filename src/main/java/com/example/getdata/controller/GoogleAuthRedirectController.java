package com.example.getdata.controller;


import com.example.getdata.configuration.SessionKey;
import com.example.getdata.configuration.FitnessAccessDeniedException;
import com.example.getdata.service.oauth.OauthTokenService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.stream.Stream;

@Controller
public class GoogleAuthRedirectController {

    private final OauthTokenService oauthTokenService;

    public GoogleAuthRedirectController(OauthTokenService oauthTokenService) {
        this.oauthTokenService = oauthTokenService;
    }

    @RequestMapping("/oauth2/callback/google")
    public String callbackUrl(HttpServletRequest request, HttpSession httpSession)
    {
        String code = request.getParameter("code");
        String accessDenied = request.getParameter("access_denied") == null
                ? "" : request.getParameter("access_denied");
        if (!accessDenied.isBlank()) throw new FitnessAccessDeniedException("Authorization from google failed");
        String error = request.getParameter("error") == null
                ? "" : request.getParameter("error");
        if (!error.isBlank()) throw new FitnessAccessDeniedException("Authorization from google failed");
        String[] scopes = request.getParameter("scope").split(" ");
        if (code.isBlank()) throw new FitnessAccessDeniedException("Authorization from google failed");
        String scopeWithFitnessPermission =
                Stream.of(scopes)
                        .filter(s -> s.contains("fitness"))
                        .findFirst()
                        .orElseThrow(() -> new FitnessAccessDeniedException("You must have to allow fitness data to be accessed."));
        httpSession
                .setAttribute(SessionKey.GOOGLE_OAUTH_TOKEN.toString(),
                        oauthTokenService.fetchToken(code, scopeWithFitnessPermission)
                );


        //////////////////////should redirect to my app main webpage/////////////
        return "done";
    }
}















