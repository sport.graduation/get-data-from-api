package com.example.getdata.dto.primitives;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Point implements Serializable {
    private long startTimeNanos;
    private long endTimeNanos;
    private String dataTypeName;
    private String originDataSourceId;
    private List<FloatValue> value;
}
