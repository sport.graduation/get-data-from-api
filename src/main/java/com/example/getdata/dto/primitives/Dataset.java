package com.example.getdata.dto.primitives;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Dataset implements Serializable {
    private String dataSourceId;
    private List<Point> point;
}
