package com.example.getdata.dto.primitives;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FloatValue implements Serializable {
    private float fpVal;
    private int intVal;
    private List<MapVal> mapVal;
}
