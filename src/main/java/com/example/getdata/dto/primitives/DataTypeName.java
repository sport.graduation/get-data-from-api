package com.example.getdata.dto.primitives;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DataTypeName implements Serializable {
    private String dataTypeName;

}
