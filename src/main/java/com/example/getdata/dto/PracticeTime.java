package com.example.getdata.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PracticeTime {
    private long startTimeMillis;
    private long endTimeMillis;

}
