package com.example.getdata.dto.request;

import com.example.getdata.dto.primitives.DataTypeName;
import com.example.getdata.dto.primitives.DurationMillis;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestAggregatedData implements Serializable {

    private List<DataTypeName> aggregateBy;
    private long startTimeMillis;
    private long endTimeMillis;
    private DurationMillis bucketByTime;

}
