package com.example.getdata;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
//@EnableJpaRepositories
@EnableEurekaClient


public class GetDataFromApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetDataFromApiApplication.class, args);

	}

	@Bean("restTemplate1")
	@LoadBalanced
	@Primary
	public RestTemplate restTemplate1() {
		return new RestTemplate();
	}


	@Bean("restTemplate2")
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public Sampler samplerOb() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
