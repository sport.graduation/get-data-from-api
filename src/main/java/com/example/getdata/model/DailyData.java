package com.example.getdata.model;


import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class DailyData implements Serializable {

    private long id;
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private double relaxedHeartRate;
    private double lightIntensityHeartRate;
    private double moderateIntensityHeartRate;
    private double vigorousIntensityHeartRate;
    private double sleepHours;
}
