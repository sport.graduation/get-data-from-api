package com.example.getdata.model;


import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class PracticeData implements Serializable {

    private long id;
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private int totalDistance;
    private double maxSpeed;
    private int burnedCalories ;
    private int numOfSprints ;
    private double hsr ;
    private int avgHeartRate;
    private int maxHeartRate;
    private int minHeartRate;
    private int redZoneDuration;
}
